This project is targeting to make a simple to use gem for enabling a Ruby/Rails project to 
have the ability to send SMS message via AWS service in the short developing time.

# Installation
In the Gemfile add the following ~

gem 'sns_helper'

# Usage
Need to setup environment variables in the server you use this gem:

Bash shell ~

    export AWS_ACCESS_KEY_ID=what_you_got_from_AWS
    export AWS_SECRET_ACCESS_KEY=what_you_got_from_AWS


Assume that you already installed the gem and setup the environment variables. You can test the function in your local irb console.

After entered the console, run the following command will send the sms message to the mobile number you input.

    irb> require 'sns_helper'
    irb> service = SnsHelper.new
    irb> service.send_message('your_target_number', 'message you want to send')

Please be careful about the format of the target mobile number, currently it needs to be in +8869xxxxxxxx format.
The AWS credential you use needs to be able to use AWS SNS service.

*Warning: sending sms will have charge to the AWS account you use, rate varies between telecoms.*