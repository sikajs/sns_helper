require 'minitest/autorun'
require 'sns_helper'

class SnsHelperTest < Minitest::Test
  def test_hello
    assert_equal 'This is hello world from sns_helper!', SnsHelper.hi
  end
end
