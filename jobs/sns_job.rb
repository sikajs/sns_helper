# This can be an example to make a background job to send sms message in your project
class SnsJob < ApplicationJob
  def perform(phone_number, message)
    service = SnsHelper.new
    service.send_message(phone_number, message)
  end
end
