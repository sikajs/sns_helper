require 'aws-sdk'

# This is the main file of sns_helper gem
class SnsHelper
  attr_reader :sns

  # say hi to the world
  #
  # Example:
  # >> SnsHelper.hi
  # => This is hello world from sns_helper!
  #
  def self.hi
    'This is hello world from sns_helper!'
  end

  # By initialize a SnsHelper, you can then simply use the send_message method to deliver your send_message
  # Remember to set up the aws creditical for AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
  #
  # Example:
  # >> service = SnsHelper.new
  # => #<SnsHelper:0x00007fe6ee435e40 @sns=#<Aws::SNS::Client>>
  #
  def initialize(region: 'ap-northeast-1')
    @sns = Aws::SNS::Client.new(
      region: region,
      access_key_id: ENV['AWS_ACCESS_KEY_ID'],
      secret_access_key: ENV['AWS_SECRET_ACCESS_KEY']
    )
  end

  def sms_attr
    sns.get_sms_attributes
  end

  def sms_type=(type: 'Promotional')
    return false unless %w[Promotional Transactional].include?(type)
    sns.set_sms_attributes(
      attributes: {
        DefaultSMSType: type
      }
    )
  end

  # Send the message to a mobile phone_number
  #
  # Example:
  # >> service = SnsHelper.new
  # >> service.send_message('+886912456789', 'This is just a test message')
  #
  def send_message(phone_number, message)
    if valid_phone_number(phone_number)
      sns.publish(phone_number: phone_number, message: message)
    else
      false
    end
  end

  # The current pattern is only for Taiwan
  def valid_phone_number(phone_number)
    return false unless /^\+8869\d{8}/ =~ phone_number
    true
  end
end
