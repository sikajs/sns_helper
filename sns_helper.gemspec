Gem::Specification.new do |s|
  s.name = 'sns_helper'
  s.version = '0.0.3'
  s.date = '2018-08-26'
  s.summary = 'A simple gem to get the project working with AWS SNS service'
  s.description = 'Just a simple gem to get the project working with AWS SNS service'
  s.authors = ['Jason Shen']
  s.email = 'sikajs@gmail.com'
  s.files = ['lib/sns_helper.rb', 'jobs/sns_job.rb']
  s.homepage = 'https://gitlab.com/sikajs/sns_helper'
  s.license = 'MIT'
  s.add_dependency 'aws-sdk-rails', ['~> 2']
  s.add_dependency 'aws-sdk-sns', ['~> 1.3']
end
